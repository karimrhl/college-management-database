
/* Finding the names of the students  that have not registered 
in at least 3 different courses for the first semester */

SELECT firstname,lastname
FROM student
WHERE id IN 
	(SELECT studentid
	FROM studentcourse
	WHERE MONTH(startDate) = 08 and DAY(startDate) = 25
	GROUP BY studentid
	HAVING COUNT(DISTINCT courseid) < 3);