CREATE DATABASE  IF NOT EXISTS College;
USE College;


DROP TABLE if exists department;
create table department
(
	id  INT   NOT NULL AUTO_INCREMENT,
    departmentName varchar(30) NULL,
    primary key (id)
);



DROP TABLE if EXISTS course;
CREATE TABLE course
(
    Cid         INT         NOT NULL AUTO_INCREMENT,
    Employee_ID INT 		NOT NULL,
    courseName VARCHAR(50) NULL,
    deptid     INT         NULL,
    PRIMARY KEY (Cid)
);


DROP TABLE if EXISTS student;

CREATE TABLE student
(
    id           INT         NOT NULL AUTO_INCREMENT,
    firstname    VARCHAR(30) NULL,
    lastname     VARCHAR(50) NULL,
    street       VARCHAR(50) NULL,
    streetDetail VARCHAR(30) NOT NULL,
    city         VARCHAR(30) NOT NULL,
    state        VARCHAR(30) NOT NULL,
    postalCode   CHAR(5)     NOT NULL,
    majorid      INT         NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS Employee;
CREATE TABLE Employee
(
	Employee_ID varchar(11), 
	Employee_First_Name varchar(50),
	Employee_Last_Name varchar(100),
	PRIMARY KEY (Employee_ID)
);


DROP TABLE IF EXISTS Instructor;
CREATE TABLE Instructor
(
	Employee_ID int, 
	Emplyee_First_Name varchar(50),
	Employee_Last_Name varchar(100),
	gender varchar(50), 
	instructor_email varchar(100),
	instructor_address varchar(100),
	PRIMARY KEY (Employee_ID)
);

DROP TABLE IF EXISTS Admin_staff;
CREATE TABLE Admin_staff
(
	Employee_ID int, 
	Emplyee_First_Name varchar(50),
	Employee_Last_Name varchar(100),
	Address varchar(200), 
	Admin_Email varchar(100),
	Admin_gender varchar(50),
	occupation  varchar(100),
PRIMARY KEY (Employee_ID)
);


DROP TABLE if EXISTS studentCourse;
CREATE TABLE studentCourse
(
    studentid INT  NULL,
    courseid  INT  NULL,
    progress  INT  NULL,
    startDate DATE NULL,
    FOREIGN KEY (studentid) REFERENCES student (id),
    FOREIGN KEY (courseid) REFERENCES course (Cid)
);

DROP TABLE IF EXISTS Parents;
CREATE TABLE Parents
(
	Parent_ID varchar(11),
	Parent_address varchar(200), 
	P_gender varchar(50),
	PRIMARY KEY (Parent_ID)
);

DROP TABLE IF EXISTS Alumni;
CREATE TABLE Alumni
(
	Al_ID varchar(15), 
	Al_name varchar(100),
    Al_surname varchar(100),
	Al_Email varchar(100),
	PRIMARY KEY (Al_ID)
 );

DROP TABLE IF EXISTS student_parents;
 CREATE TABLE Student_Parents
 (
	Parent_ID int,
	id int,
	P_first_name varchar(100),
	P_last_name varchar(100),
	PRIMARY KEY(Parent_ID, id)
 );
 
  DROP TABLE IF EXISTS Hire;
 CREATE TABLE Hire
 (
	school_id int,
	Employee_ID int, 
	PRIMARY KEY(school_id, Employee_ID)
 );

  DROP TABLE IF EXISTS conduct_reports;
 CREATE TABLE College.conduct_reports
 (
	Employee_ID int, 
	Parent_ID int,
	PRIMARY KEY(Employee_ID,Parent_ID)
 );

DROP TABLE IF EXISTS Advices;
 CREATE TABLE Advices
 (
	Al_ID VARCHAR(40), 
	Sid int,
	PRIMARY KEY (Al_ID, Sid)
 );
 