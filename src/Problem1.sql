/*
names of student that have progress < 50 including in which course
*/
select  student.firstname, student.lastname,studentcourse.progress, course.courseName, P_first_name, P_last_name
from (((studentcourse join student on studentId=id) join course on studentcourse.courseId=course.Cid)) 
join student_parents on student_parents.id=studentID
where studentcourse.progress<50
limit 5;