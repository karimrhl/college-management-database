/* Finds the best student for each course*/

SELECT student.firstname, student.lastname, MAX(studentCourse.progress) AS 'Progress', course.courseName
from (((studentcourse join student on studentid=id)join course on studentcourse.courseid=course.Cid))
group by course.courseName;