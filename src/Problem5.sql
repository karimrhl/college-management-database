/* selecting the students that are qualified to have a meeting with alumni students to discuss for a future job opportunity */

SELECT student.firstname, student.lastname, Alumni.Al_name, Alumni.AL_Email, 
Advices.Sid,studentcourse.progress, studentcourse.CourseId, course.courseName
FROM ((((studentcourse join student on studentId=id)join course on    
       studentcourse.courseId=course.Cid)  
       join Advices on Sid=id) join alumni on Advices.Al_id=Alumni.AL_id) 
WHERE studentcourse.progress>85;