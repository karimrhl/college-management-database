USE College;

/* Get the percentage of students who get more than 55 average
 progress on their courses */
 
SELECT ( COUNT(studentid) )/(SELECT COUNT(DISTINCT studentid) FROM studentcourse) * 100 as 'Percentage'
FROM (SELECT studentid, AVG(progress) as average
	FROM studentcourse
	GROUP BY studentid
	HAVING average > 55) AS T;