
/* Find the names of students enrolled in the maximum number of courses */ 
SELECT DISTINCT student.firstname, student.lastname, count(studentcourse.CourseId) as 'number of courses'
FROM student join studentcourse on student.id = studentcourse.StudentId
WHERE student.id IN (SELECT studentcourse.studentid
			         FROM studentcourse
                     GROUP BY studentcourse.studentid
                     HAVING COUNT(*) >= ALL (SELECT COUNT(*)
									         FROM studentcourse 
											 GROUP BY studentcourse.studentid));
                                     
/* to check if query is working run the following queries 							
 select id 
from student 
where firstname= "Johana" and lastname="Jolley";
 to check how many courses is johana jolley taking 
(SELECT COUNT(*)
FROM studentcourse 
where StudentId=43);
/* finally to check that is right you can see in the data studentcourse that it is the student taking the max numbers of courses */



