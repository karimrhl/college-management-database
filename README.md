# College Management Database

The organization structure of a Department in a College is quite complicated and requires allot of effort to manage.
In our database project, we found solutions to improve many things in a department of a specific college.

## Getting started

Instructions to Execute the Files in the Folder:

First create the tables for each class by executing the tables.sql, then to insert the data execute the data.sql. Finally, the queries are imlemented in seperate files

## Step 1: tables
Write and apply the CREATE scripts
Use the above diagram to devise CREATE statements to create the tables required for this project. Order your CREATE statements as needed so that all foreign key definitions execute without error. Please think about each field and add NOT NULL constraints where appropriate.

## Step 2: Data
The provided INSERT script data must be applied to your database. If the scripts you wrote in step 1 are incorrect, these statements may not execute correctly, so you must then revise and re-apply your DDL statements until you succeed with the insertions.

You will not be able to proceed with this assignment until all INSERT statements succeed.

## Step 3: Appetizers

Write queries that return the following results:
A. The enrollment team is gathering analytics about student enrollment and especially during summer they now want
to know about August admissions specifically. Because  their was a confusion between giving courses during
summer or not. So, they asked if they can know how many student have courses during August.

B. Next, to have data on the level of their students for advertisement purposes, the advertisement team wanted to
know the percentage of their students that got an average mark higher than 55.

C. The college has 76 different courses. Being the best student is a hard job, but not impossible, and we know
that in each course there is a student who is the best. This student could be the best in different courses, but not
necessary. That is what we did here, we found the best student for every course.

## Step 4: Problem Tackles

D. sometimes students don't  progress in their courses and most of the time Parents are not knowing about this situation, so the mission here was to select the students having a progress < 50 and to know the specific course name and the name of parents of those students, so the department can contact them.

E. Due to the requests from student for an increase in breaks between courses, the department decided to get the
student which has the maximum number of courses and check this amount of courses so they can decide what to
do.

F. In our college, the students have elective courses and optional courses.The registration problem is very common among students in the college, where the student doesn’t choose enough courses for his first semester. So we decided to find the students who have still not registered to at least 3 courses, so someone can notify them about this issue.




